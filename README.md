MRoot项目简介


MRoot是基于Spring Boot2使用Kotlin编写的快速开发平台（100%兼容Java，可以与Java互操作）

代码生成器，可以生成业务90%的代码，可快速完成某个业务的开发

使用Maven对项目进行模块化管理，提高项目的易开发性、扩展性


具有如下特点

灵活的权限控制，可控制权限到按钮级别

完善的角色管理及数据权限

完善的XSS防范及脚本过滤，防止表单重复提交

友好的代码结构及注释，便于阅读及二次开发

Quartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能

前端使用Bootstrap，优美的页面，丰富的插件


主要功能

数据库：Druid数据库连接池，监控数据库访问性能，统计SQL的执行性能

持久层：MyBatis持久化，使用MyBatis-Plus优化，减少sql开发量，使用Hibernate Validation进行数据验证

MVC：基于Spring Mvc注解，Rest风格Controller，Exception统一管理

任务调度：Spring+Quartz, 可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能

基于Spring的国际化信息

Shiro进行权限控制

缓存：注解缓存数据

自定义线程池、异步任务

日志：logback打印日志，存入数据库，同时基于时间和文件大小分割日志文件

工具类：加密解密、字符串处理等等

代码自动生成，可生成90%的业务代码


技术选型

开发语言：Kotlin

核心框架：Spring Boot2

数据库连接池：Alibaba Druid

持久层框架：MyBatis + MyBatis-Plus

安全框架：Apache Shiro

任务调度：Spring + Quartz

缓存框架：Ehcache3

日志管理：SLF4J、Logback

验证框架：Hibernate Validation

模板：Freemarker

前端框架：Bootstrap


开发环境

JDK10+、Maven3.5.2+、Mysql8+、Spring Boot2


本地部署

通过git下载源码

创建数据库 mroot ，数据库编码为 UTF-8

执行 doc/db/mroot.sql文件

修改application-XXX.properties文件，更新MySQL账号和密码

IDEA运行MRootAdminApplication.kt，则可启动项目

mroot-admin访问路径：你的IP地址:端口/login

账号密码：admin/123456


已完成功能

用户管理：配置用户角色

角色管理：配置角色所拥有的权限

菜单管理：配置角色所拥有的权限

操作日志：系统操作日志记录和查询

代码生成：可生成90%的业务代码

缓存管理：Ehcache3

文章管理：百度编辑器的上传处理


分支介绍

分支	    介绍

master	    稳定版本，推荐使用

prod        历史版本

dev	        开发分支，bug修复分支


在线体验

传送门： http://mroot.yuneryu.com/login
