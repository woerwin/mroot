<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">


<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">

        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    ${I18N("message.table.head.title")}
                </h3>
            </div>
        </div>

    </div>

    <div class="m-portlet__body">

        <div class="row">
            <div class="col-xl-12">

            <#-- 工具栏开始 -->
                <div class="m-section">
                    <div class="m-section__content">

                    <#-- 搜索工具栏开始 -->
                        <div class="m-form m-form--label-align-right m--margin-top-20">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">

                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left search-form">
                                                <input type="text" class="form-control m-input"
                                                       name="table" value="${table}"
                                                       placeholder="${I18N('message.generateCode.list.title.text')}"
                                                       autocomplete="off">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
                                                    </span>
                                            </div>
                                            <div class="d-md-none m--margin-bottom-10"></div>
                                        </div>
                                        <div class="col-md-8">
                                            <button id="search" data-url="${index}"
                                                    class="btn btn-primary m-btn m-btn--icon" type="button">
                                                        <span><i class="fa fa-check"></i>
                                                            <span>${I18N("message.table.search.btn")}</span>
                                                        </span>
                                            </button>
                                        </div>
                                        <div class="d-md-none m--margin-bottom-10"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <#-- 搜索工具栏结束 -->

                    </div>

                </div>
            <#-- 工具栏结束 -->

            <#-- 表格开始 -->
                <div class="m-datatable m-datatable--default">
                    <table id="sample" class="table table-bordered table-hover m-table m-table--head-bg-brand">
                        <thead>
                        <tr>
                            <th style="width:1%;">
                                ${I18N("message.table.id")}
                            </th>
                         <#assign th=[
                         "message.generateCode.table.name",
                         "message.generateCode.table.createTime",
                         "message.generateCode.table.comment",
                         "message.table.operate.title"
                         ]/>
                            <@tableTh th></@tableTh>
                        </tr>
                        </thead>
                        <tbody>
    <#if page.records??  && (0 < page.records?size)>
        <#list page.records as item>
                <tr>
                    <td>${item_index + 1}</td>
                    <td>${item.tableName}</td>
                    <td>${item.createTime}</td>
                    <td>
                     <span data-toggle="m-tooltip" data-placement="top"
                           title="<@defaultStr item.tableComment></@defaultStr>">
                                <@subStr str=item.tableComment length=item.tableComment?length></@subStr>
                     </span>
                    </td>
                    <td>
                        <a class="btn btn-primary m-btn m-btn--icon m-btn--pill"
                           href="${generate}/${item.tableName}">
                            <i class="fa fa-edit"></i>&nbsp;${I18N("message.generateCode.table.btn")}
                        </a>
                    </td>
                </tr>
        </#list>
    <#else>
                        <tr>
                            <td class="text-center" colspan="5">${I18N("message.table.empty.content")}</td>
                        </tr>
    </#if>

                        </tbody>
                    </table>

                <#-- 分页开始 -->
                    <div id="paginate"
                         class="m-datatable__pager m-datatable--paging-loaded clearfix"></div>
                <#-- 分页结束 -->
                </div>
            <#-- 表格结束 -->

            </div>
        </div>

    </div>

</div>


</@OVERRIDE>
<#include "/default/scriptPlugin/index.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {

        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');

        // 分页
        Table.pagination({
            url: '${index}',
            totalRow: '${page.total}',
            pageSize: '${page.size}',
            pageNumber: '${page.current}'
        });

    });
</script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
